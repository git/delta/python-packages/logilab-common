Source: logilab-common
Section: python
Priority: optional
Maintainer: Logilab S.A. <contact@logilab.fr>
Uploaders: David Douard <david.douard@logilab.fr>,
	   Alexandre Fayolle <afayolle@debian.org>,
	   Sandro Tosi <morph@debian.org>,
	   Adrien Di Mascio <Adrien.DiMascio@logilab.fr>,
	   Nicolas Chauvat <nicolas.chauvat@logilab.fr>,
Build-Depends:
 debhelper (>= 8),
 python3-typing-extensions,
 dh-python,
 tox,
 git,
 python3-all,
 python3-setuptools,
Build-Depends-Indep:
 graphviz,
 python3-tz | python3 (<< 3.3),
X-Python3-Version: >= 3.3
Standards-Version: 3.9.1
Homepage: http://www.logilab.org/project/logilab-common
Vcs-Hg: http://hg.logilab.org/logilab/common
Vcs-Browser: http://hg.logilab.org/logilab/common

Package: python3-logilab-common
Architecture: all
Depends:
 ${python3:Depends},
 ${misc:Depends},
#Recommends:
# python3-egenix-mxdatetime,
Suggests:
 python3-kerberos,
Description: useful miscellaneous modules used by Logilab projects
 logilab-common is a collection of low-level Python packages and modules,
 designed to ease:
 .
  * handling command line options and configuration files
  * writing interactive command line tools
  * manipulation files and character strings
  * running unit tests
  * manipulating tree structures
  * generating text and HTML reports
  * logging
 .
 This package contains the Python 3.x version of this library.
